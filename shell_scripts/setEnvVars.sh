#!/bin/bash
# Shell script to set env variables
echo " "
echo "***** 1. Setting Environment *****"
#[ -f ~/.bashrc ] && rm -- ~/.bashrc
read -p "Enter Environment (DEV/SIT/STAGE/PROD): " TEST_ENV
case $TEST_ENV in
	dev|DEV)
		echo "Setting to DEV Enironment"
		export TEST_ENV="DEV"
		export APP_URL="gorest.co.in"
		;;
	sit|SIT)
		echo "Setting to SIT Enironment"
		export TEST_ENV="SIT"
		export APP_URL="gorest.co.in"
		;;
	stage|STAGE)
		echo "Setting to STAGE Enironment"
		export TEST_ENV="STAGE"
		export APP_URL="gorest.co.in"
		;;
	prod|PROD)
		echo "Setting to PROD Enironment"
		export TEST_ENV="PROD"
		export APP_URL="gorest.co.in"
		;;
	*)
		echo "Setting to DEV Enironment"
		export TEST_ENV="dev"
		export APP_URL="gorest.co.in"
		;;
esac
