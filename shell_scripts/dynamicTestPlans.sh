#!/bin/bash
# Shell script to list Test Plans

#Collect the files in the array $FILES
FILES=(test_plans/*)

#Enable extending globbing, which lets us use @(foo|bar) to match either 'foo' or 'bar'
shopt -s extglob

#Start building the string to match agains
FILE_NAME_LIST="@(${FILES[0]}"

#Add the rest of the files to string
for((i=0;i<${#FILES[@]};i++))
do
	FILE_NAME_LIST+="|$(basename -s .jmx ${FILES[$i]})"
done

#Close the parenthesis. $FILE_NAME_LIST is now @(file1||file2|..|fileN)
FILE_NAME_LIST+=")"

#Get timestamp
TIMESTAMP=$(date "+%Y-%m-%d-%H-%M-%S")

#Show the menu. This will list all files and string "Quit"
select FILE in $(basename -s .jmx "${FILES[@]}") "Quit"
do
	case $FILE in
	#If the choice is one of the files (if it matches $FILE_NAME_LIST)
		$FILE_NAME_LIST)
			echo "***** 3. Executing Test *****"
			echo "Environment: " ${TEST_ENV^^}
			echo "Application URL: " ${APP_URL}
			echo "Executing Test Plan: "$FILE 
			sh ./lib/apache-jmeter-5.4/bin/jmeter.sh -Jenv.app.url=${APP_URL} -n -t test_plans/$FILE.jmx -l test_results/$FILE.${TEST_ENV^^}.$TIMESTAMP.jtl
			echo "***** 4. Generating Report *****"
			sh ./lib/apache-jmeter-5.4/bin/jmeter.sh -g test_results/$FILE.${TEST_ENV^^}.$TIMESTAMP.jtl -o test_reports/$FILE.${TEST_ENV^^}.$TIMESTAMP/
			break
			;;
		"Quit")
			echo "Terminating Test !!!"
			break
			;;
		*)
			echo "Invalid Selection $REPLY - Please choose a number from 1 to $((${#FILES[@]}+1))"
			;;
	esac
done
	