#!/bin/bash
# Shell script to set env variables

echo "*************************************"
echo "***** Executing API/ Load Tests *****"
echo "*************************************"

#Build env for non-login shell
source ./shell_scripts/setEnvVars.sh

#Set JMeter Variables
export JVM_ARGS="-Djava.net.preferIPv4Stack=true"
export JVM_ARGS="-Xms2048m -Xmx4096m -XX:NewSize=1024m -XX:MaxNewSize=2048m"
export JVM_ARGS="-Denv.app.url=${APP_URL}"

echo " "
echo "***** 2. Select Test Plan *****"

source ./shell_scripts/dynamicTestPlans.sh

echo " "
echo "***** API/ Load Tests Completed *****"
