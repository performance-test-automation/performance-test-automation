# performance-test-automation

## Introduction:
API/Performance Test Automation Framework has been created to create, manage and execute API or Load Test for REST / GraphQL endpoints.

## Setup
### Pre-requisites
* Installation of:
    * Chocolatey
    * Java JDK/JRE 1.8
    * Git Bash (Win)
    * JMeter
* Configure JAVA_HOME & JRE_HOME
* Configure JMeter in PATH env variable

### Steps
* Open Powershell and Install JMeter

    ```choco install jmeter```

* Open GitBash and Configure JMeter in PATH env variable

    ```export PATH=$PATH:/c/ProgramData/chocolatey/lib/jmeter/tools/apache-jmeter-5.2.1/bin```

* Create local folder (say, C:\develop) and clone repo:

    ```git clone git@gitlab.com:performance-test-automation/performance-test-automation.git```

## Execute Scripts

* CLI: Open Git Bash, execute below commands and follow on-screen instructions:

    ```$ cd /c/develop/performance-test-automation```

    ```$ sh ./run.sh```

    ```
        $ sh ./run.sh
        *************************************
        ***** Executing API/ Load Tests *****
        *************************************

        ***** 1. Setting Environment *****
        Enter Environment (DEV/SIT/STAGE/PROD): prod
        Setting to PROD Enironment

        ***** 2. Select Test Plan *****
        1) E2E_API_Test_Plan
        2) Performance_Test_Plan
        3) Quit
        #? 2
        ***** 3. Executing Test *****
        Environment:  PROD
        Application URL:  gorest.co.in
        Executing Test Plan: Performance_Test_Plan
        Creating summariser <summary>
        Created the tree successfully using test_plans/Performance_Test_Plan.jmx
        Starting standalone test @ Thu Jun 03 02:01:59 EDT 2021 (1622700119988)
        Waiting for possible Shutdown/StopTestNow/HeapDump/ThreadDump message on port 4445
        Warning: Nashorn engine is planned to be removed from a future JDK release
        summary +    255 in 00:00:30 =    8.6/s Avg:   581 Min:   253 Max:  1693 Err:    47 (18.43%) Active: 10 Started: 10 Finished: 0
        summary +    451 in 00:00:30 =   15.0/s Avg:   665 Min:   636 Max:   695 Err:    89 (19.73%) Active: 10 Started: 10 Finished: 0
        summary =    706 in 00:01:00 =   11.8/s Avg:   635 Min:   253 Max:  1693 Err:   136 (19.26%)
        summary +    449 in 00:00:30 =   15.0/s Avg:   665 Min:   521 Max:  1529 Err:    90 (20.04%) Active: 10 Started: 10 Finished: 0
        summary =   1155 in 00:01:30 =   12.9/s Avg:   647 Min:   253 Max:  1693 Err:   226 (19.57%)
        summary +    451 in 00:00:30 =   15.0/s Avg:   666 Min:   639 Max:   693 Err:    91 (20.18%) Active: 10 Started: 10 Finished: 0
        summary =   1606 in 00:02:00 =   13.4/s Avg:   652 Min:   253 Max:  1693 Err:   317 (19.74%)
        summary +    163 in 00:00:11 =   15.0/s Avg:   666 Min:   634 Max:   694 Err:    31 (19.02%) Active: 0 Started: 10 Finished: 10
        summary =   1769 in 00:02:11 =   13.5/s Avg:   653 Min:   253 Max:  1693 Err:   348 (19.67%)
        Tidying up ...    @ Thu Jun 03 02:04:10 EDT 2021 (1622700250940)
        ... end of run
        ***** 4. Generating Report *****

        ***** API/ Load Tests Completed *****

        
    ```

## Create API/Load Test Plan/Scripts

* Open Git Bash or Powershell and type jmeter

* JMeter UI will open

* Make a copy of any existing Test Plan and modify accordingly
