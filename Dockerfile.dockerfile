#Dockerfile for running JMeter tests in Gitlab CI Runner
FROM ubuntu:latest 
LABEL author=robhattarai@outlook.com 

RUN apt-get autoremove
RUN apt-get clean
RUN apt-get autoclean
RUN apt-get -o Acquire::Max-FutureTime=86400 -y update
RUN apt-get install -y unzip
RUN apt-get install -y jq
RUN apt-get install -y wget

#Java
RUN apt-get install -y default-jre
RUN apt-get install java-common
#RUN wget https://corretto.aws/downloads/latest/amazon-corretto-8-x64-linux-jdk.deb
#RUN dpkg --install amazon-corretto-8-x64-linux-jdk.deb
#RUN export JAVA_HOME=/usr/lib/jvm/java-1.8.0-amazon-corretto
#RUN export JRE_HOME=/usr/lib/jvm/java-1.8.0-amazon-corretto/jre
 
#JMeter
RUN wget https://archive.apache.org/dist/jmeter/binaries/apache-jmeter-5.4.zip
RUN unzip -o apache-jmeter-5.4 -d /usr/lib/
RUN export PATH=$PATH:/usr/lib/apache-jmeter-5.4/bin
RUN ./usr/lib/apache-jmeter-5.4/bin/jmeter.sh --version


CMD [“echo”,”Docker image created: ubuntu-gitlab-runner-performance-test-automation”]
