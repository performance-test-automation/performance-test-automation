#!/bin/bash
# Shell script to set env variables
echo " "
echo "***** 1. Setting Environment *****"
[ -f ~/.bashrc ] && rm -- ~/.bashrc
#read -p "Enter Environment (DEV/SIT/STAGE/PROD): " TEST_ENV
case $TEST_ENV in
	dev|DEV)
		echo "Setting to DEV Enironment"
		export TEST_ENV="DEV"
		echo 'export TEST_ENV="DEV"' >> ~/.bashrc
		export APP_URL="gorest.co.in"
		echo 'export APP_URL="gorest.co.in"' >> ~/.bashrc
		;;
	sit|SIT)
		echo "Setting to SIT Enironment"
		export TEST_ENV="SIT"
		echo 'export TEST_ENV="SIT"' >> ~/.bashrc
		export APP_URL="gorest.co.in"
		echo 'export APP_URL="gorest.co.in"' >> ~/.bashrc
		;;
	stage|STAGE)
		echo "Setting to STAGE Enironment"
		export TEST_ENV="STAGE"
		echo 'export TEST_ENV="STAGE"' >> ~/.bashrc
		export APP_URL="gorest.co.in"
		echo 'export APP_URL="gorest.co.in"' >> ~/.bashrc
		;;
	prod|PROD)
		echo "Setting to PROD Enironment"
		export TEST_ENV="PROD"
		echo 'export TEST_ENV="PROD"' >> ~/.bashrc
		export APP_URL="gorest.co.in"
		echo 'export APP_URL="gorest.co.in"' >> ~/.bashrc
		;;
	*)
		echo "Setting to DEV Enironment"
		export TEST_ENV="DEV"
		echo 'export TEST_ENV="DEV"' >> ~/.bashrc
		export APP_URL="gorest.co.in"
		echo 'export APP_URL="gorest.co.in"' >> ~/.bashrc
		;;
esac
