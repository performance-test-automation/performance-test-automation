#!/bin/bash
# Shell script to set env variables

echo "*************************************"
echo "***** Executing API/ Load Tests *****"
echo "*************************************"

#Install pre-requisites
chmod a+x ./.gitlab-ci/shell_scripts/installPackages.sh
/bin/bash ./.gitlab-ci/shell_scripts/installPackages.sh


#Build env for non-login shell
/bin/bash ./.gitlab-ci/shell_scripts/setEnvVarsCI.sh

#Set JMeter Variables
export JVM_ARGS="-Djava.net.preferIPv4Stack=true"
export JVM_ARGS="-Xms2048m -Xmx4096m -XX:NewSize=1024m -XX:MaxNewSize=2048m"
export JVM_ARGS="-Denv.app.url=${APP_URL}"

echo " "
echo "***** 2. Selecting Test Plan *****"

/bin/bash ./.gitlab-ci/shell_scripts/execE2ETest.sh

echo " "
echo "***** API/ Load Tests Completed *****"
