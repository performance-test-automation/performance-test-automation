#!/bin/bash
# Shell script to execute test

#Get timestamp
TIMESTAMP=$(date "+%Y-%m-%d-%H-%M-%S")

source ~/.bashrc

echo "***** 3. Executing Test *****"
echo "Environment: " ${TEST_ENV^^}
echo "Application URL: " ${APP_URL}

FILE="E2E_Test_Plan"

echo "Executing Test Plan: "$FILE 

export PATH=$PATH:/usr/lib/apache-jmeter-5.4/bin

jmeter -Jenv.app.url=${APP_URL} -n -t test_plans/$FILE.jmx -l test_results/$FILE.${TEST_ENV^^}.$TIMESTAMP.jtl

echo "***** 4. Generating Report *****"

jmeter -g test_results/$FILE.${TEST_ENV^^}.$TIMESTAMP.jtl -o test_reports/$FILE.${TEST_ENV^^}.$TIMESTAMP/
