#!/bin/bash
# Shell script to set env variables
echo " "
echo "***** 0. Installing pre-requisites *****"
apt-get autoremove
apt-get clean
apt-get autoclean
apt-get -o Acquire::Max-FutureTime=86400 -y update
apt-get install -y unzip
apt-get install -y jq
apt-get install -y wget

#Java
apt-get install -y default-jre
apt-get install -y java-common
#wget https://corretto.aws/downloads/latest/amazon-corretto-8-x64-linux-jdk.deb
#dpkg --install amazon-corretto-8-x64-linux-jdk.deb
#export JAVA_HOME=/usr/lib/jvm/java-1.8.0-amazon-corretto
#export JRE_HOME=/usr/lib/jvm/java-1.8.0-amazon-corretto/jre
 
#JMeter
wget https://archive.apache.org/dist/jmeter/binaries/apache-jmeter-5.4.zip
unzip -o apache-jmeter-5.4 -d /usr/lib/
export PATH=$PATH:/usr/lib/apache-jmeter-5.4/bin
jmeter --version
